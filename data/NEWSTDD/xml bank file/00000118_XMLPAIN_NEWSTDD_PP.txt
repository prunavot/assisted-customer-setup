<?xml version="1.0" encoding="UTF-8" standalone="no" ?>
<Document xmlns="urn:iso:std:iso:20022:tech:xsd:pain.001.001.03">
	<CstmrCdtTrfInitn>
		<GrpHdr>
			<MsgId>INTC32200088</MsgId>
			<CreDtTm>2020-06-08T15:20:45</CreDtTm>
			<NbOfTxs>1</NbOfTxs>
			<CtrlSum>128814.40</CtrlSum>
			<InitgPty>
				<Nm>WZNYSOJJM B.K.</Nm>
				<Id>
					<OrgId>
						<BICOrBEI>BEIJOB21</BICOrBEI>
					</OrgId>
				</Id>
			</InitgPty>
		</GrpHdr>
		<PmtInf>
			<PmtInfId>INTC32200088</PmtInfId>
			<PmtMtd>TRF</PmtMtd>
			<BtchBookg>false</BtchBookg>
			<NbOfTxs>1</NbOfTxs>
			<CtrlSum>128814.40</CtrlSum>
			<PmtTpInf>
				<InstrPrty>NORM</InstrPrty>
				<CtgyPurp>
					<Cd>TREA</Cd>
				</CtgyPurp>
			</PmtTpInf>
			<ReqdExctnDt>2020-06-10</ReqdExctnDt>
			<Dbtr>
				<Nm>WZNYSOJJM B.K.</Nm>
				<PstlAdr>
					<StrtNm>E/ LXPSQRH YJEJ VUBNFM 46, 3j KSVVLU.</StrtNm>
					<PstCd>89</PstCd>
					<TwnNm>TRITRF</TwnNm>
					<Ctry>ES</Ctry>
				</PstlAdr>
			</Dbtr>
			<DbtrAcct>
				<Id>
					<IBAN>ES1882384613956010443244</IBAN>
				</Id>
				<Ccy>EUR</Ccy>
			</DbtrAcct>
			<DbtrAgt>
				<FinInstnId>
					<BIC>BNPAESMSXXX</BIC>
					<PstlAdr>
						<Ctry>ES</Ctry>
					</PstlAdr>
				</FinInstnId>
			</DbtrAgt>
			<UltmtDbtr>
				<Nm>DMHCFGJGN B.G.</Nm>
			</UltmtDbtr>
			<ChrgBr>SHAR</ChrgBr>
			<CdtTrfTxInf>
				<PmtId>
					<InstrId>322VINI000049</InstrId>
					<EndToEndId>0020000154</EndToEndId>
				</PmtId>
				<Amt>
					<InstdAmt Ccy="EUR">128814.40</InstdAmt>
				</Amt>
				<CdtrAgt>
					<FinInstnId>
						<BIC>BNPAFRPPXXX</BIC>
						<PstlAdr>
							<Ctry>FR</Ctry>
						</PstlAdr>
					</FinInstnId>
				</CdtrAgt>
				<Cdtr>
					<Nm>YBBEXA</Nm>
					<PstlAdr>
						<StrtNm>00, DKOONQ UK KOFHPN</StrtNm>
						<PstCd>08783</PstCd>
						<TwnNm>TUQMOZ FRGQV 6</TwnNm>
						<Ctry>FR</Ctry>
					</PstlAdr>
					<Id>
						<OrgId>
							<Othr>
								<Id>49166889300010</Id>
							</Othr>
						</OrgId>
					</Id>
				</Cdtr>
				<CdtrAcct>
					<Id>
						<IBAN>FR7662464438912554034172968</IBAN>
					</Id>
					<Ccy>EUR</Ccy>
				</CdtrAcct>
				<RmtInf>
					<Ustrd>/ADV/0020000154 10.6.2020</Ustrd>
				</RmtInf>
			</CdtTrfTxInf>
		</PmtInf>
	</CstmrCdtTrfInitn>
</Document>
